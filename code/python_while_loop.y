%{
    #include <stdio.h>
    #include <stdlib.h>

    int yylex();
    int yyerror();

    extern FILE *yyin;
    extern FILE *yyout;

%}

%token WHILE AND OR NTEQ EQ LT LTEQ GT GTEQ ID NUMBER TB PRINT NEWLINE COLON
%left AND OR
%left LT GT LTEQ GTEQ EQ NTEQ

%%
start : S {
        printf("Valid\n");
        return 0;
    }
    ;

S : WHILE E COLON NEWLINE statement
    ;

statement : statement NEWLINE
        | TB PRINT E
        ;
  
E : E NTEQ E
    | '(' E ')'
    | E EQ E
    | E LT E 
    | E LTEQ E
    | E GT E
    | E GTEQ E
    | E AND E
    | E OR E
    | ID
    | NUMBER
    ;
%%

int yyerror(char const *s){
    printf("\n %s\n",s);
    return 1;
}

int main(){
    //yyin = fopen("demo.py", "r");
    //yyout = fopen("x.py","w");
    yyparse();
    return 0;
}
