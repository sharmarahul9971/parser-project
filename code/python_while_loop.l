%{
    #include <string.h>
    #include "y.tab.h"
    void yyerror(char *); 
%}

%%
"while"                  { return WHILE; }
"print"                  { return PRINT; }
"and"                    { return AND; }
"or"                     { return OR; }
[0-9]+                   { return NUMBER; }
[a-zA-Z_][a-zA_Z0-9_]*   { return ID; }
[()<>=]                  { return *yytext; }
">="                     { return GTEQ; }
"<="                     { return LTEQ; }
"=="                     { return EQ; }
"!="                     { return NTEQ; }
":"                      { return COLON; }
\n                       { return NEWLINE; }
\t                       { return TB; }
[ ]+                     { ; }
.                        { yyerror(strcat(yytext," - unknown character")); exit(0);}
%%

int yywrap(void){
    return 1;
}
